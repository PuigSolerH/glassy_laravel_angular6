# Glassy

Glassy is an app made with MVC structure with Laravel and Angular6
The main purpose of this appliaction is to get into those programming languages at class and to make a project using them.


## Getting Started

This is a simple application with some user funcionality such as Login, Register, Social Login, Listing Products and filtering them, some
interactions with the map with some shops where we catch that data from MongoDB server.


### Prerequisites

First of all you will need to execute the following commant to install what am I using in this app.

MySQL ubuntu
```
sudo apt-get install -y mysql-server
```

NPM
```
sudo apt-get install npm
```

Angular-cli ubuntu
```
sudo npm install -g @angular/cli
```


### Installing

You will need to execute the follwing commands to run the development env once you have installed the prerequisites.

On client

```
cd frontend
npm install
ng build
ng serve
```

On server

```
cd backend
composer install
cp .env.testing .env
php artisan key:generate
php artisan jwt:generate
php artisan migrate
php artisan serve
```

*My app is isomorphic so the following commands will be executed on 'backend' directory:

```
gulp
```
```
npm run dev
```


## Built With

* [Laravel](https://laravel.com/) - As a backend framework
* [Angular](https://angular.io/) - As a frontend framework
* [NPM](https://www.npmjs.com/) - Dependency Management
* [PHPMyAdmin](https://www.phpmyadmin.net/) - DB used
* [Babel](https://babeljs.io/) - JavaScript compiler


## Authors

* **Hibernon Puig** - [Puigsolerh](https://github.com/puigsolerh)


## Extra

This app is incomplete (like everyonse should be). So I could have include extra functionality such as favorite list, most rated glasses, graphQL, cart and so on.
