<?php 

namespace App\Http\Controllers\Api;

use App\Glasses;
use App\Http\Requests\Api\DeleteGlasses;
use App\RealWorld\Paginate\Paginate;
use App\RealWorld\Filters\GlassesFilter;
use App\RealWorld\Transformers\GlassesTransformer;

class GlassesController extends ApiController 
{ 	
    /**
     * GlassesController constructor.
     *
     * @param GlassesTransformer $transformer
    */
    public function __construct(GlassesTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @param GlassesFilter $filter
     * @return \Illuminate\Http\Response
     */
    public function index(GlassesFilter $filter)
    {
        $glasses = new Paginate(Glasses::loadRelations()->filter($filter));

        return $this->respondWithPagination($glasses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Glasses $glasses)
    {
        return $this->respondWithTransformer($glasses);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteGlasses $request, Glasses $glasses)
    {
        $glasses->delete();

        return $this->respondSuccess();
    }

}