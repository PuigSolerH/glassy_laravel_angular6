<?php

namespace App\Http\Controllers\Api;

use Auth;
use Storage;
use App\LoginSocialUser;
use App\User;
use App\Exceptions\SocialAuthException;
use App\Http\Requests\Api\LoginUser;
use App\Http\Requests\Api\RegisterUser;
use App\RealWorld\Transformers\UserTransformer;

class AuthController extends ApiController
{
    /**
     * AuthController constructor.
     *
     * @param UserTransformer $transformer
     */

    protected $loginUser;

    public function __construct(UserTransformer $transformer, LoginSocialUser $loginUser)
    {
        $this->transformer = $transformer;
        $this->loginUser = $loginUser;
    }

    /**
     * Login user and return the user if successful.
     *
     * @param LoginUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginUser $request)
    {
        $credentials = $request->only('user.email', 'user.password');
        $credentials = $credentials['user'];

        if (! Auth::once($credentials)) {
            return $this->respondFailedLogin();
        }

        return $this->respondWithTransformer(auth()->user());
    }

    /**
     * Register a new user and return the user if successful.
     *
     * @param RegisterUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterUser $request)
    {
        $user = User::create([
            'username' => $request->input('user.username'),
            'email' => $request->input('user.email'),
            'password' => $request->input('user.password'),
        ]);

        return $this->respondWithTransformer($user);
    }

    // Metodo encargado de la redireccion
    public function redirectToProvider($provider)
    {
        return $this->loginUser->authenticate($provider);
    }

    // Metodo encargado de obtener la información del usuario
    public function handleProviderCallback($provider)
    {
        try {
            $social_user = $this->loginUser->login($provider);
            if ($user = User::where('username', $social_user->name)->where('email', $social_user->email)->first()) {
                return $this->authAndRedirect($user); // Login y redirección
            } else { 
                // En caso de que no exista creamos un nuevo usuario con sus datos.
                $user = User::create([
                    'username' => $social_user->name,
                    'email' => $social_user->email,
                    'image' => $social_user->avatar,
                    'password' => '',
                ]);
    
                return $this->authAndRedirect($user); // Login y redirección
            }
        } catch (SocialAuthException $e) {
            echo 'Not User';
        }
    }

    // Login y redirección
    public function authAndRedirect($user)
    {
        Storage::disk('local')->put('file.txt', $this->respondWithTransformer($user));

        return redirect()->to('http://localhost:4200/sociallogin');
    }

    public function loginSocial()
    {
        $exists = Storage::disk('local')->exists('file.txt');

        if($exists){
            $user = Storage::disk('local')->get('file.txt');
        }
        return explode('GMT', $user)[1];
    }

}
