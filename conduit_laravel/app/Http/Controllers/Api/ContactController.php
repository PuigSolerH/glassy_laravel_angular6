<?php 
namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Contact;

use Mail;

class ContactController extends ApiController { 	
    public function contact(Contact $request){
        $data = array(
            'name' => $request->input('body.name'),
            'email' => $request->input('body.email'),
            'opinion' => $request->input('body.opinion'),
        );
        
        $sent = Mail::send('email.email', $data, function ($message){
            $message->subject('Test email');
            $message->from('hiber98.test@gmail.com', 'puigsolerh');
            $message->to('hiber98@gmail.com');
        });
        if($sent) dd("something wrong"); //var_dump + exit
        
        return response()->json(['message' => 'Request completed']);
        //echo json_encode("Your email has been sent successfully");
        //return "Your email has been sent successfully";
    }
}