<?php
namespace App;
use App\Exceptions\SocialAuthException;
use Exception;
use Socialite;

class LoginSocialUser
{
    public function authenticate($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    public function login($provider)
    {
        $socialUserInfo = Socialite::driver($provider)->stateless()->user();
        return $socialUserInfo;
        
    }
}