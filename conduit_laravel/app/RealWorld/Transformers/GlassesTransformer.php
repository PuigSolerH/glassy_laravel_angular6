<?php

namespace App\RealWorld\Transformers;

class GlassesTransformer extends Transformer
{
    protected $resourceName = 'glasses';

    public function transform($data)
    {
        return [
            'slug' => $data['slug'],
            'name' => $data['name'],
            'desc' => $data['desc'],
            'type' => $data['type'],
            'material' => $data['material'],
            'gender' => $data['gender'],
            'price' => $data['price'],
            'img' => $data['img'],
            'stock' => $data['stock'],
            'shopList' => $data['shopList'],
            'author' => [
                'username'  => $data['user']['username'],
                'bio'       => $data['user']['bio'],
                'image'     => $data['user']['image'],
            ]
        ];
    }
}