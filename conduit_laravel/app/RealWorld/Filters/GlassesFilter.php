<?php

namespace App\RealWorld\Filters;

use App\Shop;

class GlassesFilter extends Filter
{
    /**
     * Filter by shop name.
     * Get all the glasses tagged by the given shop name.
     *
     * @param $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function shop($name)
    {
        $shop = Shop::whereName($name)->first();

        $glassesIds = $shop ? $shop->glasses()->pluck('glasses_id')->toArray() : [];

        return $this->builder->whereIn('id', $glassesIds);
    }
}