<?php

namespace App;

use App\RealWorld\Slug\HasSlug;
use App\RealWorld\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

class Glasses extends Model
{
    use Filterable, HasSlug;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'desc',
        'type',
        'material',
        'gender',
        'price',
        'img',
        'stock'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'shops'
    ];

    /**
     * Get the list of shops attached to the glasses.
     *
     * @return array
     */
    public function getShopListAttribute()
    {
        return $this->shops->pluck('name')->toArray();
    }

    /**
     * Load all required relationships with only necessary content.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLoadRelations($query)
    {
        // print_r($query->pluck('name'));        

        // return $query->with(['user.followers' => function ($query) {
        //         $query->where('follower_id', auth()->id());
        //     }])
        //     ->with(['favorited' => function ($query) {
        //         $query->where('user_id', auth()->id());
        //     }])
        //     ->withCount('favorited');
    }

    /**
     * Get all the glasses that belong to the shop.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */    
    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

  
    /**
     * Get the key name for route model binding.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get the attribute name to slugify.
     *
     * @return string
     */
    public function getSlugSourceColumn()
    {
        return 'name';
    }
}