<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id'     => '121503878518-svohj9ca1a9lr30fvlv2i26p3qquvdl8.apps.googleusercontent.com',
        'client_secret' => 'wwImz69eHEKj4xmpWVJM8sPY',
        'redirect'      => env('GOOGLE_URL'),
    ]

    // 'facebook' => [
    //     'client_id' => '774663719548815',
    //     'client_secret' => '9e0e120713c4f6f5be02194c59c5c870',
    //     'redirect' => 'http://localhost:8000/api/auth/facebook/callback'
    // ]
];
