<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

            $table->unique('name');
        });

        Schema::create('glasses_shop', function (Blueprint $table) {
            $table->unsignedInteger('glasses_id');
            $table->unsignedInteger('shop_id');

            $table->primary(['glasses_id', 'shop_id']);

            $table->foreign('glasses_id')
                ->references('id')
                ->on('glasses')
                ->onDelete('cascade');

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('glasses_shop');
        Schema::dropIfExists('shops');
    }
}
