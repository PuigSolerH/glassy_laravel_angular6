<?php

use Illuminate\Database\Seeder;

class DummyDataSeeder extends Seeder
{
    /**
     * Total number of users.
     *
     * @var int
     */
    protected $totalUsers = 25;

    /**
     * Total number of glasses.
     *
     * @var int
     */
    protected $totalGlasses = 25;

    /**
     * Total number of glasses.
     *
     * @var int
     */
    protected $totalShops = 25;

    /**
     * Maximum glasses that can be created by a user.
     *
     * @var int
     */
    protected $maxGlassesByUser = 5;

    /**
     * Percentage of users with glasses.
     *
     * @var float Value should be between 0 - 1.0
     */
    protected $userWithGlassesRatio = 0.8;

    /**
     * Maximum tags that can be attached to an article.
     *
     * @var int
     */
    protected $maxGlassesShops = 5;


    /**
     * Populate the database with dummy data for testing.
     * Complete dummy data generation including relationships.
     * Set the property values as required before running database seeder.
     *
     * @param \Faker\Generator $faker
     */
    public function run(\Faker\Generator $faker)
    {
        $users = factory(\App\User::class)->times($this->totalUsers)->create();

        $shops = factory(\App\Shop::class)->times($this->totalShops)->create();

        $users->random((int) $this->totalUsers * $this->userWithGlassesRatio)
            ->each(function ($user) use ($faker, $shops) {
                $user->glasses()
                    ->saveMany(
                        factory(\App\Glasses::class)
                        ->times($faker->numberBetween(1, $this->maxGlassesByUser))
                        ->make()
                    )
                    ->each(function ($glasses) use ($faker, $shops) {
                        $glasses->shops()->attach(
                            $shops->random($faker->numberBetween(1, min($this->maxGlassesShops, $this->totalShops)))
                        );
                    });
            });
    }
}
