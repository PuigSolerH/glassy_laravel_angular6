<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {

    Route::post('users/login', 'AuthController@login');
    Route::post('users', 'AuthController@register');

    Route::get('user', 'UserController@index');
    Route::match(['put', 'patch'], 'user', 'UserController@update');

    Route::get('profiles/{user}', 'ProfileController@show');
    Route::post('profiles/{user}/follow', 'ProfileController@follow');
    Route::delete('profiles/{user}/follow', 'ProfileController@unFollow');

    Route::get('articles/feed', 'FeedController@index');
    Route::post('articles/{article}/favorite', 'FavoriteController@add');
    Route::delete('articles/{article}/favorite', 'FavoriteController@remove');

    Route::resource('articles', 'ArticleController', [
        'except' => [
            'create', 'edit'
        ]
    ]);

    Route::resource('articles/{article}/comments', 'CommentController', [
        'only' => [
            'index', 'store', 'destroy'
        ]
    ]);

    Route::get('tags', 'TagController@index');

    Route::post('contact', 'ContactController@contact');

    Route::get('glasses/{glasses}', 'GlassesController@show');
    
    Route::delete('glasses/{glasses}', 'GlassesController@destroy');

    Route::resource('glasses', 'GlassesController', [
        'except' => [
            'create', 'store', 'edit', 'update' 
        ]
    ]);

    Route::resource('shops', 'ShopController', [
        'except' => [
            'create', 'store', 'show', 'edit', 'update', 'destroy' 
        ]
    ]);

    Route::get('auth/{provider}', 'AuthController@redirectToProvider')->name('social.auth');
    Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');
    Route::get('/sociallogin', 'AuthController@loginSocial');
    
});