<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ShopTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_an_array_of_shops()
    {
        $shops = factory(\App\Shop::class)->times(5)->create();

        $response = $this->getJson('/api/shops');

        $response->assertStatus(200)
            ->assertJson([
                'shops' => $shops->pluck('name')->toArray()
            ]);
    }

    /** @test */
    public function it_returns_an_empty_array_of_shops_when_there_are_none_in_database()
    {
        $response = $this->getJson('/api/shops');

        $response->assertStatus(200)
            ->assertJson([
                'shops' => []
            ]);

        $this->assertEmpty($response->json()['shops'], 'Expected empty shops array');
    }
}