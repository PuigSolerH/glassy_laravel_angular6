<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class GlassesTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_returns_an_array_of_glasses()
    {
        $glasses = factory(\App\Glasses::class)->times(5)->create();

        $response = $this->getJson('/api/glasses');

        $response->assertStatus(200)
            ->assertJson([
                'glasses' => $glasses->toArray()
            ]);
    }

    /** @test */
    public function it_returns_an_empty_array_of_glasses_when_there_are_none_in_database()
    {
        $response = $this->getJson('/api/glasses');

        $response->assertStatus(200)
            ->assertJson([
                'glasses' => []
            ]);

        $this->assertEmpty($response->json()['glasses'], 'Expected empty glasses array');
    }
}