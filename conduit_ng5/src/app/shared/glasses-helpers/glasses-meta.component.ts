import { Component, Input } from '@angular/core';

import { Glasses } from '../../core';

@Component({
  selector: 'app-glasses-meta',
  templateUrl: './glasses-meta.component.html'
})
export class GlassesMetaComponent {
  @Input() glasses: Glasses;
}
