import { Component, Input } from '@angular/core';

import { Glasses, GlassesListConfig, GlassesService } from '../../core';
@Component({
  selector: 'app-glasses-list',
  templateUrl: './glasses-list.component.html'
})
export class GlassesListComponent {
  constructor (
    private glassesService: GlassesService
  ) {}

  @Input() limit: number;
  @Input()
  set config(config: GlassesListConfig) {
    if (config) {
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  query: GlassesListConfig;
  results: Glasses[];
  loading = false;
  currentPage = 1;
  totalPages: Array<number> = [1];

  setPageTo(pageNumber) {
    this.currentPage = pageNumber;
    this.runQuery();
  }

  runQuery() {
    this.loading = true;
    this.results = [];

    // Create limit and offset filter (if necessary)
    if (this.limit) {
      this.query.filters.limit = this.limit;
      this.query.filters.offset =  (this.limit * (this.currentPage - 1));
    }

    this.glassesService.query(this.query)
    .subscribe(data => {
      this.loading = false;
      this.results = data.glasses;

      // Used from http://www.jstips.co/en/create-range-0...n-easily-using-one-line/
      this.totalPages = Array.from(new Array(Math.ceil(data.glassesCount / this.limit)), (val, index) => index + 1);
    });
  }

  onDeleteGlasses(glasses) {
    this.glassesService.destroy(glasses.slug)
      .subscribe(
        success => {
          location.reload();
        }
      );
  }

}