import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Glasses, UserService, User } from '../../core';

@Component({
  selector: 'app-glasses-preview',
  templateUrl: './glasses-preview.component.html'
})
export class GlassesPreviewComponent {
  constructor(
    private userService: UserService
  ) {}

  @Input() glasses: Glasses;
  @Output() deleteGlasses = new EventEmitter<boolean>();

  canModify: boolean;
  
  ngOnInit() {
    // Load the current user's data
    this.userService.currentUser.subscribe(
      (userData: User) => {
        this.canModify = (userData.username === this.glasses.author.username);
      }
    );
  }

  deleteClicked() {
    this.deleteGlasses.emit(true);
  }
}