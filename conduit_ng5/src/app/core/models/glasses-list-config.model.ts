export interface GlassesListConfig {
    type: string;
  
    filters: {
      shop?: string,
      author?: string,
      favorited?: string,
      limit?: number,
      offset?: number
    };
  }