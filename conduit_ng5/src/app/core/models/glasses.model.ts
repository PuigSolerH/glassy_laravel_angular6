import { Profile } from './profile.model';

export interface Glasses {
    slug: string;
    name: string;
    desc: string;
    type: string;
    material: string;
    gender: string;
    price: string;
    img: string;
    stock: number;
    shopList: string;
    // shopList: string[];
    author: Profile;
  }
  