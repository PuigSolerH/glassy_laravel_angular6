import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { Glasses, GlassesListConfig } from '../models';
import { map } from 'rxjs/operators';

@Injectable()
export class GlassesService {
  constructor (
    private apiService: ApiService
  ) {}

  query(config: GlassesListConfig): Observable<{glasses: Glasses[], glassesCount: number}> {
    // Convert any filters over to Angular's URLSearchParams
    const params = {};

    Object.keys(config.filters)
    .forEach((key) => {
      params[key] = config.filters[key];
    });

    return this.apiService
    .get(
      '/glasses' + ((config.type === 'feed') ? '/feed' : ''),
      new HttpParams({ fromObject: params })
    );
  }

  get(slug): Observable<Glasses> {
    return this.apiService.get('/glasses/' + slug)
    .pipe(map(data => data.glasses));
  }

  destroy(glassesSlug) {
    return this.apiService.delete('/glasses/' + glassesSlug);
  }

}