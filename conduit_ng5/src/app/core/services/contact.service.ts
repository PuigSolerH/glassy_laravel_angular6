import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { Contact } from '../models';
import { map } from 'rxjs/operators';

@Injectable()
export class ContactService {
  constructor (
    private apiService: ApiService
  ) {}

  save(contact): Observable<Contact> {
    return this.apiService
    .post(`/contact`, { body: contact }
    ).pipe(map(data => data.body));
  }

}