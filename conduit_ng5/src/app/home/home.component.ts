import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlassesListConfig, ShopsService } from '../core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(
    private router: Router,
    private shopsService: ShopsService
  ) {}

  isAuthenticated: boolean;
  listConfig: GlassesListConfig = {
    type: 'all',
    filters: {}
  };
  shops: Array<string> = [];
  shopsLoaded = false;

  ngOnInit() {
    this.shopsService.getAll()
    .subscribe(shops => {
      this.shops = shops;
      this.shopsLoaded = true;
    });
  }

  setListTo(type: string = '', filters: Object = {}) {
    // If feed is requested but user is not authenticated, redirect to login
    // if (type === 'feed' && !this.isAuthenticated) {
    //   this.router.navigateByUrl('/login');
    //   return;
    // }

    // Otherwise, set the list object
    this.listConfig = {type: type, filters: filters};
    // console.log(this.listConfig);
  }
}
