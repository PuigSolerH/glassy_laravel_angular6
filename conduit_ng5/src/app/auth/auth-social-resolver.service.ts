import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
// import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { UserService } from '../core';
import { map ,  take } from 'rxjs/operators';
// import { take } from 'rxjs/operators';

@Injectable()
export class SocialLogin implements Resolve<any> {
  constructor(
    private router: Router,
    private userService: UserService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this.userService.social()
        .pipe(map(
          data => {
            this.router.navigateByUrl('/');
          }
        ));
  }

}