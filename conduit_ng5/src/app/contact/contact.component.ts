import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { Errors, Contact, ContactService } from '../core';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
    selector: 'app-contact-page',
    templateUrl: './contact.component.html'
})
export class ContactComponent {
    contact: Contact = {} as Contact;
    contactForm: FormGroup;
    errors: Errors = {errors: {}};
    isSubmitting = false;

    constructor(
        private contactService: ContactService,
        // private route: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder,
        public toastr: ToastrManager
    ) {
        // use the FormBuilder to create a form group
        this.contactForm = this.fb.group({
          name: ['', Validators.required],
          email: ['', Validators.email],
          opinion: ['', Validators.minLength(20)]
        });
    }
    
    submitForm() {
        this.isSubmitting = true;
        this.errors = {errors: {}};

        // update the model
        this.updateContact(this.contactForm.value);

        // post the changes
        this.contactService.save(this.contact).subscribe(
            contact => {
                this.router.navigateByUrl('/'),
                this.toastr.successToastr('This is success toast.', 'Success!');
            },
            err => {
                this.errors = err;
                this.toastr.errorToastr('This is error toast.', 'Oops!');
                this.isSubmitting = false;
            }
        );
    }
    
    updateContact(values: Object) {
        Object.assign(this.contact, values);
    }

}