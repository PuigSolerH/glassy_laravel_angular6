import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GlassesComponent } from './glasses.component';
// import { ArticleCommentComponent } from './article-comment.component';
import { GlassesResolver } from './glasses-resolver.service';
// import { MarkdownPipe } from './markdown.pipe';
import { SharedModule } from '../shared';
import { GlassesRoutingModule } from './glasses-routing.module';

@NgModule({
  imports: [
    SharedModule,
    GlassesRoutingModule
  ],
  declarations: [
    GlassesComponent,
    // ArticleCommentComponent,
    // MarkdownPipe
  ],

  providers: [
    GlassesResolver
  ]
})
export class GlassesModule {}
