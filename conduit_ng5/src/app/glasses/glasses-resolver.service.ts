import { Injectable, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { GlassesService, Glasses } from '../core';
import { catchError } from 'rxjs/operators';

@Injectable()
export class GlassesResolver implements Resolve<Glasses> {
  constructor(
    private glassesService: GlassesService,
    private router: Router,
    // private userService: UserService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    // console.log(route.params);
    return this.glassesService.get(route.params['slug'])
      .pipe(catchError((err) => this.router.navigateByUrl('/')));
  }
}
