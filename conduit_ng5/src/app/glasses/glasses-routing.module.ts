import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GlassesComponent } from './glasses.component';
import { GlassesResolver } from './glasses-resolver.service';

const routes: Routes = [
  {
    path: ':slug',
    component: GlassesComponent,
    resolve: {
      glasses: GlassesResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GlassesRoutingModule {}
