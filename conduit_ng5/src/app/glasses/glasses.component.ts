import { Component, OnInit } from '@angular/core';
import { GlassesListConfig} from '../core';
import { ActivatedRoute, Router } from '@angular/router';
import { Glasses } from '../core';

@Component({
  selector: 'app-glasses-page',
  templateUrl: './glasses.component.html'
})
export class GlassesComponent implements OnInit {
  glasses: Glasses;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  listConfig: GlassesListConfig = {
    type: 'all',
    filters: {}
  };

  ngOnInit() {
    // Retreive the prefetched glasses
    this.route.data.subscribe(
      (data: { glasses: Glasses }) => {
        this.glasses = data.glasses;
        this.listConfig.filters.shop = this.glasses.shopList;
        console.log(this.glasses);
      }
    );
  }

}
